/*ANKAN MUKHERJEE*/
/*Ping Pong Game with Speed Control*/

/*Defining the pins*/
#define LED_LEFT 8
#define RED_LED_1 9
#define RED_LED_2 10
#define RED_LED_3 11
#define RED_LED_4 12
#define LED_RIGHT 13
#define INTERRUPT_LEFT 2
#define INTERRUPT_RIGHT 3

/*Defining the global variables*/
unsigned long start_time; //Time when button activates for hitting
unsigned long left_end_time; //Time when left button is pushed
unsigned long right_end_time; //Time when right button is pushed
unsigned long del_time; //Time elapsed between the ball reaching the player and the player hitting it back
float interval; //Time interval between the transfer of ball from one LED to the next
float speed_factor; //Maximum factor by which the ball accelerates
boolean left_hit_enabled; //Checks if the ball has reached the player on the left
boolean right_hit_enabled; //Checks if the ball has reached the player on the right
boolean left_winner; //Checks if the player on the left has won
boolean right_winner; //Checks if the player on the right has won
boolean game_over; //Checks if a player has won
boolean game_on; //Checks if the game is on
boolean left_side;
boolean right_side;


void setup() 
{
  Serial.begin(9600);
  DDRB|=B00111111; //Set pins 8 to 13 to OUTPUT mode
  attachInterrupt(digitalPinToInterrupt(INTERRUPT_LEFT),leftButton,RISING); //Rising edge detection for left button
  attachInterrupt(digitalPinToInterrupt(INTERRUPT_RIGHT),rightButton,RISING); //Rising edge detection for right button
  
  /*Initialization of variables*/
  
  left_hit_enabled=true;
  right_hit_enabled=false;
  left_winner=false;
  right_winner=false;
  game_on=false;
  game_over=false;;
  interval=600.0; //We have set the interval to 0.6s; one can tweak it at his/her own free will
  speed_factor=1.2;
  left_side=false;
  right_side=false;
}


/*Ball moving from left to right*/
void leftToRight()
{
  PORTB=B00000010; //Pin 9, i.e., the first LED from the left is turned ON, others are turned OFF
  delay(interval);
  PORTB=B00000100; //Pin 10, i.e., the second LED from the left is turned ON, others are turned OFF
  delay(interval);
  PORTB=B00001000; //Pin 11, i.e., the third LED from the left is turned ON, others are turned OFF
  delay(interval);
  PORTB=B00010000; //Pin 12, i.e., the fourth LED from the left is turned ON, others are turned OFF
  right_hit_enabled=true; //Ball reaches the right end, activate the right button
  start_time=millis(); //Start the timer for the player on the right
  delay(interval);
}

/*Ball moving from right to left*/
void rightToLeft()
{
  PORTB=B00010000; //Pin 12, i.e., the fourth LED from the left is turned ON, others are turned OFF
  delay(interval);  
  PORTB=B00001000; //Pin 11, i.e., the third LED from the left is turned ON, others are turned OFF
  delay(interval);
  PORTB=B00000100; //Pin 10, i.e., the second LED from the left is turned ON, others are turned OFF
  delay(interval);
  PORTB=B00000010; //Pin 9, i.e., the first LED from the left is turned ON, others are turned OFF  
  left_hit_enabled=true; //Ball reaches the left end, activate the left button
  start_time=millis(); //Start the timer for the player on the right
  delay(interval);
}

/*ISR on pressing the left button*/
void leftButton()
{
  left_end_time=millis(); //Ends the timer as soon as the left button is pressed
  if(!game_on) //To start the game if not already started
  {
      game_on=true;
      left_side=true;
  }
  if(left_hit_enabled) //Checks if the button were pressed at the correct time, i.e., after the ball reached the left end
  {
    left_hit_enabled=false;
  }
}

/*ISR on pressing the left button*/
void rightButton()
{
  
  right_end_time=millis(); //Ends the timer as soon as the right button is pressed
  if(!game_on) //To start the game if not already started
  {
      game_on=true;
      right_side=true;
  }
  if(right_hit_enabled)
  {
    right_hit_enabled=false; //Checks if the button were pressed at the correct time, i.e., after the ball reached the right end
  }
}


/*Set of instructions for Game Over*/
void gameOver()
{
  if(left_winner) //If the player on the left wins, left green LED blinks 4 times
  {
    for(int i=1;i<=4;i++)
    {
      PORTB=B00000000;
      delay(200);
      PORTB=B00000001;
      delay(200);
    }
    left_winner=false; //Resets the winning status to false so that LED does not keep blinking
  }
  if(right_winner) //If the player on the right wins, right green LED blinks 4 times
  {
    for(int i=1;i<=4;i++)
    {
      PORTB=B00000000;
      delay(200);
      PORTB=B00100000;
      delay(200);
    }
    right_winner=false; //Resets the winning status to false so that LED does not keep blinking
  }
  /*Note that the above section of code in this method will execute only after someone has won*/

  /*Resetting the parameters to their default values for the next game*/
  
  PORTB=B00000000;
  game_on=false;
  left_hit_enabled=true;
  right_hit_enabled=true;
  interval=600;
  left_side=false;
  right_side=false;
}


/*The actual running of the game*/
void loop() 
{
  //Checks if the game is on so that the ball can be passed from left to right
  if(game_on && left_side)
  {
    right_side=true;
    leftToRight(); //Ball travels across the board from left to right
    del_time=right_end_time-start_time; //Calculates the time taken by the player on the right to hit the ball
    if(del_time>interval) //If the time taken to hit the ball exceeds the interval, it is game over
    {
      game_on=false;
      left_winner=true;
    }
    else
    {
      PORTB=B00100000; //Blink the right green LED for successful hit      
      delay(100);
      interval=interval-(speed_factor-1)*del_time; //If hit immediately upon receiving, speed is same. If hit after some delay, speed is increased linearly till a factor of speed_factor
    }
  }

  //Checks if the game is on and it is not the first pass (in the first pass, the ball always travels from left to right) so that the ball can be passed from right to left
  if(game_on && right_side)
  {
    left_side=true;
    rightToLeft(); //Ball travels across the board from right to left
    del_time=left_end_time-start_time; //Calculates the time taken by the player on the right to hit the ball
    if(del_time>interval) //If the time taken to hit the ball exceeds the interval, it is game over
    {
      game_on=false;
      right_winner=true;
    }
    else
    {
      PORTB=B00000001; //Blink the right green LED for successful hit 
      delay(100);
      interval=interval-(speed_factor-1)*del_time; //If hit immediately upon receiving, speed is same. If hit after some delay, speed is increased linearly till a factor of speed_factor
    }
    Serial.println(interval);
  }
  if(!game_on) //If game is not on, that means it has ended
  {
    gameOver();
  }
}
