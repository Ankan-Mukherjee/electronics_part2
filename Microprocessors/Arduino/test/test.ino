
#include <Servo.h>

Servo servo;
int light=40;
int lim=12;

void setup() {
  Serial.begin(9600); 
  servo.attach(8);
  servo.write(0);
}

void loop() {
  light=analogRead(A3);
  Serial.println(light);
  if(light<lim)
  {
    servo.write(32);
    delay(200);
    servo.write(20);
    delay(50);
  }
}
