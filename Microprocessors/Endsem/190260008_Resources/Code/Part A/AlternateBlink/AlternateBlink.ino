/*
 * PH 435 ENDSEM
 * FESTIVAL LIGHTS
 * ANKAN MUKHERJEE
 * 190260008
 */

/*
 * We attempt to build a circuit and code that will enable us to perform the functions of festival lights.
 */

 int time_delay;

void setup() 
{
   DDRD|=B11110000; //Set pins 4-7 as output
   DDRB|=B00001111; //Set pins 8-11 as output
   PORTD&=B00001111; 
   PORTB&=B11110000;
   //Initially, turn all LEDs OFF
   time_delay=200; //Initializing time delay to 0
}

void timeDelay()
{
  delay(time_delay);
}


void alternateBlink()
{
    PORTB=(PORTB&B11110000)|B00001010;
    PORTD=(PORTD&B00001111)|B10100000;   
    //Turn LEDs 1,3,5,7 ON
    
    timeDelay(); //Delay appropriately
  
    PORTB=(PORTB&B11110000)|B00000101;
    PORTD=(PORTD&B00001111)|B01010000;    
    //Turn LEDs 2,4,6,8 ON
    
    timeDelay(); //Delay appropriately
  
}



void loop() 
{
  alternateBlink();
}
