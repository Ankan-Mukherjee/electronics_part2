/*
 * PH 435 ENDSEM
 * FESTIVAL LIGHTS
 * ANKAN MUKHERJEE
 * 190260008
 */

/*
 * We attempt to build a circuit and code that will enable us to perform the functions of festival lights.
 */

 #define DELAY_PIN A0
 #define BUTTON_PIN 2
 int time_delay;
 int mode;

void setup() 
{
  DDRD|=B11110000; //Set pins 4-7 as output
  DDRB|=B00001111; //Set pins 8-11 as output
   PORTD&=B00001111; 
   PORTB&=B11110000;
   //Initially, turn all LEDs OFF
   time_delay=0; //Initializing time delay to 0
   mode=0;
   attachInterrupt(digitalPinToInterrupt(BUTTON_PIN), button, RISING);
}


void loop() 
{
  /*
   * We will use a switch statement for the modes
   */
  switch(mode)
  {
    case 0:
    //All LEDs OFF
      PORTD&=B00001111;
      PORTB&=B11110000;
      break;
    case 1:
    //Simple flashing
      simpleFlash();
      break;
    case 2:
    //Alternate blinking
      alternateBlink();
      break;
    case 3:
    //Running lights
      runningLights();
      break;
  }
}
