void simpleFlash()
{
  if(mode==1)
  {
    PORTD|=B11110000;
    PORTB|=B00001111;
    //Turn all LEDs ON
    
    timeDelay(); //Delay appropriately
  }

  if(mode==1)
  {
    PORTD&=B00001111; 
    PORTB&=B11110000;
    //Turn all LEDs OFF
    timeDelay(); //Delay appropriately
  }
}

void alternateBlink()
{
  if(mode==2)
  {
    PORTB=(PORTB&B11110000)|B00001010;
    PORTD=(PORTD&B00001111)|B10100000;   
    //Turn LEDs 1,3,5,7 ON
    
    timeDelay(); //Delay appropriately
  }

  if(mode==2)
  {    
    PORTB=(PORTB&B11110000)|B00000101;
    PORTD=(PORTD&B00001111)|B01010000;    
    //Turn LEDs 2,4,6,8 ON
    
    timeDelay(); //Delay appropriately
  }
}


void runningLights()
{
  /*
   * We could have written a loop here, but execution of loop and operations will cause more delay than direct manipulation.
   * Compact and neat codes such as looping might look good in theory but in practice, direct manipulation is the chepaest
   * in terms of processing delays.
   */

  if(mode==3)
  {  
    PORTB=(PORTB&B11110000)|B00001000;
    PORTD&=B00001111; 
    //Turn LED 1 ON
    
    timeDelay(); //Delay appropriately
  }

  
  if(mode==3)
  { 
    PORTB=(PORTB&B11110000)|B00000100;
    PORTD&=B00001111;
    //Turn LED 2 ON
    
    timeDelay(); //Delay appropriately
  }

  if(mode==3)
  {   
    PORTB=(PORTB&B11110000)|B00000010;
    PORTD&=B00001111;
    //Turn LED 3 ON
    
    timeDelay(); //Delay appropriately
  }

  if(mode==3)
  {   
    PORTB=(PORTB&B11110000)|B00000001;
    PORTD&=B00001111;
    //Turn LED 4 ON
    
    timeDelay(); //Delay appropriately
  }

  if(mode==3)
  {   
    PORTB&=B11110000;
    PORTD=(PORTD&B00001111)|B10000000; 
    //Turn LED 5 ON
    
    timeDelay(); //Delay appropriately
  }

  if(mode==3)
  {     
    PORTB&=B11110000;
    PORTD=(PORTD&B00001111)|B01000000; 
    //Turn LED 6 ON
    
    timeDelay(); //Delay appropriately
  }

  if(mode==3)
  {   
    PORTB&=B11110000;
    PORTD=(PORTD&B00001111)|B00100000; 
    //Turn LED 7 ON
    
    timeDelay(); //Delay appropriately
  }

  if(mode==3)
  {   
    PORTB&=B11110000;
    PORTD=(PORTD&B00001111)|B00010000; 
    //Turn LED 8 ON
    
    timeDelay(); //Delay appropriately
  }
}
