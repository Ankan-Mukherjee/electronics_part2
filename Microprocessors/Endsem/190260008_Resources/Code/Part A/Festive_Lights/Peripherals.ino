/*
 * We will use the timeDelay function instead of delay to poll the potentiometer output to obtain time delay
 */
void timeDelay()
{
  time_delay=map(analogRead(DELAY_PIN),0,1023,100,1000);
  delay(time_delay);
}

/*
 * Called on interrupt, i.e., button press
 */
void button()
{
  mode=(mode+1)%4; //Cycles through the modes
  /*
   * Turns OFF all LEDs temporarily before moving to the next state
   */  
  PORTD&=B00001111; 
  PORTB&=B11110000;
}
