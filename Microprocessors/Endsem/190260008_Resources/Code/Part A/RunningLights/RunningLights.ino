/*
 * PH 435 ENDSEM
 * FESTIVAL LIGHTS
 * ANKAN MUKHERJEE
 * 190260008
 */

/*
 * We attempt to build a circuit and code that will enable us to perform the functions of festival lights.
 */

 int time_delay;

void setup() 
{
   DDRD|=B11110000; //Set pins 4-7 as output
   DDRB|=B00001111; //Set pins 8-11 as output
   PORTD&=B00001111; 
   PORTB&=B11110000;
   //Initially, turn all LEDs OFF
   time_delay=200; //Initializing time delay to 0
}

void timeDelay()
{
  delay(time_delay);
}


void runningLights()
{
  /*
   * We could have written a loop here, but execution of loop and operations will cause more delay than direct manipulation.
   * Compact and neat codes such as looping might look good in theory but in practice, direct manipulation is the chepaest
   * in terms of processing delays.
   */
    PORTB=(PORTB&B11110000)|B00001000;
    PORTD&=B00001111; 
    //Turn LED 1 ON
    
    timeDelay(); //Delay appropriately

  
    PORTB=(PORTB&B11110000)|B00000100;
    PORTD&=B00001111;
    //Turn LED 2 ON
    
    timeDelay(); //Delay appropriately

    PORTB=(PORTB&B11110000)|B00000010;
    PORTD&=B00001111;
    //Turn LED 3 ON
    
    timeDelay(); //Delay appropriately

    PORTB=(PORTB&B11110000)|B00000001;
    PORTD&=B00001111;
    //Turn LED 4 ON
    
    timeDelay(); //Delay appropriately

    PORTB&=B11110000;
    PORTD=(PORTD&B00001111)|B10000000; 
    //Turn LED 5 ON
    
    timeDelay(); //Delay appropriately

    PORTB&=B11110000;
    PORTD=(PORTD&B00001111)|B01000000; 
    //Turn LED 6 ON
    
    timeDelay(); //Delay appropriately

    PORTB&=B11110000;
    PORTD=(PORTD&B00001111)|B00100000; 
    //Turn LED 7 ON
    
    timeDelay(); //Delay appropriately
 
    PORTB&=B11110000;
    PORTD=(PORTD&B00001111)|B00010000; 
    //Turn LED 8 ON
    
    timeDelay(); //Delay appropriately
}

void loop() 
{
  runningLights();
}
