/*
 * PH 435 ENDSEM
 * FESTIVAL LIGHTS
 * ANKAN MUKHERJEE
 * 190260008
 */

/*
 * We attempt to build a circuit and code that will enable us to perform the functions of festival lights.
 */

 int time_delay;

void setup() 
{
   DDRD|=B11110000; //Set pins 4-7 as output
   DDRB|=B00001111; //Set pins 8-11 as output
   PORTD&=B00001111; 
   PORTB&=B11110000;
   //Initially, turn all LEDs OFF
   time_delay=200; //Initializing time delay to 0
}
void simpleFlash()
{
    PORTD|=B11110000;
    PORTB|=B00001111;
    //Turn all LEDs ON
    
    timeDelay(); //Delay appropriately

    PORTD&=B00001111; 
    PORTB&=B11110000;
    //Turn all LEDs OFF
    timeDelay(); //Delay appropriately
}

void timeDelay()
{
  delay(time_delay);
}

void loop() 
{
  simpleFlash();
}
