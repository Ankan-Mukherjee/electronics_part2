/*
 * PH 435 ENDSEM
 * BRIGHTNESS CONTROL
 * ANKAN MUKHERJEE
 * 190260008
 */

#define PWM_PIN 10
#include <Keypad.h>

float voltage;
int output_value;

const byte ROWS = 4; //four rows
const byte COLS = 4; //three columns
char keys[ROWS][COLS] = 
{
  {'1','2','3','A'},
  {'4','5','6','B'},
  {'7','8','9','C'},
  {'*','0','#','D'}
};

byte rowPins[ROWS] = {9, 8, 7, 6}; //connect to the row pinouts of the keypad
byte colPins[COLS] = {5, 4, 3, 2}; //connect to the column pinouts of the keypad

Keypad keypad = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS );

void setup()
{
  Serial.begin(9600);
  voltage=0.0;
}
  
void loop()
{
  char pressed_key = keypad.getKey();  
  if (pressed_key)
  {
    voltage=(pressed_key-48)*0.25+0.4;
    output_value=(int)(voltage/5.0*255);
    Serial.println(pressed_key);
    analogWrite(PWM_PIN, output_value);
  }
}
