/*
 * PH 435 ENDSEM
 * BRIGHTNESS CONTROL
 * ANKAN MUKHERJEE
 * 190260008
 */


#define PWM_PIN 10

float voltage;
int output_value;

void setup() 
{
  voltage=4.0;
  output_value=(int)(voltage/5.0*255);
}

void loop() 
{  
  analogWrite(PWM_PIN, output_value);
}
