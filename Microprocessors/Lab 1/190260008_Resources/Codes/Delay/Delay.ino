#define pinX 13

int start_time;
int end_time;

void setup() 
{
  Serial.begin(9600);
}

void loop() 
{
  start_time=micros();
  digitalWrite(pinX, LOW); // t=0, pinX=13 is in setup()
  Serial.print(0);
  digitalWrite(pinX, HIGH); // Drive pinX output to high (5V)
  Serial.print(1);
  Serial.print("Game over"); // t = ΔT // confirm to user
  end_time=micros();
  Serial.println("\n");
  Serial.println(end_time-start_time);
}
