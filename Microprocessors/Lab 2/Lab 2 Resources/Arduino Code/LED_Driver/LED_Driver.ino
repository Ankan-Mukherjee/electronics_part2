#define OUTPUT_PIN 3
#define INPUT_PIN A3

float V_out;
float V_in;
int i;


void setup() 
{
  //i=0;
  V_out=4;
  V_in=0;
  //TCCR2B = TCCR2B & B11111000 | B00000111;    // set timer 2 divisor to  1024 for PWM frequency of    30.64 Hz
  Serial.begin(9600);
  pinMode(OUTPUT_PIN, OUTPUT);
  pinMode(INPUT_PIN, INPUT);
}


void loop() 
{
  //V_out=(i%50)*1.0/10.0;
  analogWrite(OUTPUT_PIN, V_out/5.0*255);  
  //i++;

  V_in=analogRead(INPUT_PIN)/1023.0*5.0;
  Serial.print(V_out);  
  Serial.print("\t");  
  Serial.print(V_in); 
  Serial.println();  
  
  delay(100);                
}
