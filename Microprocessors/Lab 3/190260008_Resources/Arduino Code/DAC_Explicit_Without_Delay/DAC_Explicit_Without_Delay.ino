#define BIT_0 8
#define BIT_1 9
#define BIT_2 10
#define BIT_3 11



void setup() 
{
  Serial.begin(9600);
  pinMode(BIT_0, OUTPUT);
  pinMode(BIT_1, OUTPUT);
  pinMode(BIT_2, OUTPUT);
  pinMode(BIT_3, OUTPUT);
}


void loop() 
{
  for(long i=1;i<=100000l;i++)
   digitalWrite(BIT_3, LOW);
  for(long i=1;i<=100000l;i++)
  digitalWrite(BIT_2, LOW); 
  for(long i=1;i<=100000l;i++)
  digitalWrite(BIT_1, LOW); 
  for(long i=1;i<=100000l;i++)
  digitalWrite(BIT_0, LOW);
  for(long i=1;i<=100000l;i++)
  digitalWrite(BIT_0, HIGH);
  for(long i=1;i<=100000l;i++)
  digitalWrite(BIT_1, HIGH);
  for(long i=1;i<=100000l;i++)
  digitalWrite(BIT_2, HIGH);
}
