Version 4
SHEET 1 2340 756
WIRE 288 208 256 208
WIRE 400 208 368 208
WIRE 608 208 400 208
WIRE 848 208 816 208
WIRE 960 208 928 208
WIRE 1168 208 960 208
WIRE 1360 208 1328 208
WIRE 1472 208 1440 208
WIRE 1680 208 1472 208
WIRE 1952 208 1824 208
WIRE 2144 208 2032 208
WIRE 256 224 256 208
WIRE 816 224 816 208
WIRE 1328 224 1328 208
WIRE 400 240 400 208
WIRE 960 240 960 208
WIRE 1472 240 1472 208
WIRE 400 352 400 320
WIRE 1472 352 1472 320
WIRE 1824 352 1824 208
WIRE 400 448 400 432
WIRE 608 448 400 448
WIRE 960 448 960 320
WIRE 1168 448 960 448
WIRE 1472 448 1472 432
WIRE 1680 448 1472 448
WIRE 2144 448 1936 448
WIRE 256 464 256 304
WIRE 400 464 400 448
WIRE 816 464 816 304
WIRE 960 464 960 448
WIRE 1328 464 1328 304
WIRE 1472 464 1472 448
WIRE 1824 464 1824 432
WIRE 1936 464 1936 448
FLAG 256 464 0
FLAG 400 464 0
FLAG 816 464 0
FLAG 960 464 0
FLAG 1824 464 0
FLAG 1936 464 0
FLAG 1328 464 0
FLAG 1472 464 0
SYMBOL voltage 400 336 R0
WINDOW 3 24 96 Invisible 2
SYMATTR InstName Bit0
SYMBOL res 384 192 R90
WINDOW 0 0 56 Invisible 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R1
SYMBOL res 272 320 R180
WINDOW 0 36 76 Invisible 2
WINDOW 3 36 40 Left 2
SYMATTR InstName R2
SYMBOL res 384 224 R0
WINDOW 0 36 40 Invisible 2
SYMATTR InstName R3
SYMATTR Value 2R
SYMBOL res 944 192 R90
WINDOW 0 0 56 Invisible 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R4
SYMBOL res 832 320 R180
WINDOW 0 36 76 Invisible 2
WINDOW 3 36 40 Left 2
SYMATTR InstName R5
SYMBOL res 944 224 R0
WINDOW 0 36 40 Invisible 2
SYMATTR InstName R6
SYMATTR Value 2R
SYMBOL voltage 1824 336 R0
WINDOW 3 24 96 Invisible 2
SYMATTR InstName Bit0
SYMBOL res 2048 192 R90
WINDOW 0 0 56 Invisible 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R7
SYMBOL voltage 1472 336 R0
WINDOW 3 24 96 Invisible 2
SYMATTR InstName Bit0
SYMBOL res 1456 192 R90
WINDOW 0 0 56 Invisible 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R10
SYMBOL res 1344 320 R180
WINDOW 0 36 76 Invisible 2
WINDOW 3 36 40 Left 2
SYMATTR InstName R11
SYMBOL res 1456 224 R0
WINDOW 0 36 40 Invisible 2
SYMATTR InstName R12
SYMATTR Value 2R
TEXT 576 320 Left 2 ;V_OUT
TEXT 1136 320 Left 2 ;R_EQ
TEXT 392 192 Left 2 ;A
TEXT 952 192 Left 2 ;A
TEXT 2112 320 Left 2 ;V_OUT
TEXT 1648 320 Left 2 ;V_EQ
TEXT 1464 192 Left 2 ;A
TEXT 240 528 Left 2 ;The Original Circuit\n \nThe Bit0 Voltage takes the value V_ref \nwhen ON and 0 when OFF.
TEXT 816 528 Left 2 ;Thevenin Equivalent Resistance\n \nThe voltage is shorted.\nThe equivalent resistance\nR_EQ is clearly R and R\nin Series, which are in turn\nin parallel with 2R.\nThus R_EQ=R
TEXT 1336 528 Left 2 ;Thevenin Equivalent Voltage\n \nThe equivalent voltage is the\nvoltage at point A in the open\nload configuration.\nThus, V_EQ=V_Ref/2 when Bit0\nis ON and 0 when Bit0 is OFF.
TEXT 1832 528 Left 2 ;Thevenin Equivalent Circuit\n \nThe equivalent circuit after all\nthe replacements.
LINE Normal 608 304 608 208 2
LINE Normal 608 336 608 448 2
LINE Normal 1168 304 1168 208 2
LINE Normal 1168 336 1168 448 2
LINE Normal 2144 304 2144 208 2
LINE Normal 2144 336 2144 448 2
LINE Normal 1680 304 1680 208 2
LINE Normal 1680 336 1680 448 2
RECTANGLE Normal 704 752 192 176 2
RECTANGLE Normal 1248 752 720 176 2
RECTANGLE Normal 1744 752 1264 176 2
RECTANGLE Normal 2224 752 1760 176 2
