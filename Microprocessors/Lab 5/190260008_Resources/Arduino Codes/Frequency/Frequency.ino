/*
 * Program to compute the frequency of a given input signal
 * 
 * ANKAN MUKHERJEE
 * 190260008
 * IIT BOMBAY
 * PH 230
 * LAB 05
 * Code 01
 */

#include <TimerOne.h>

#define INPUT_PIN 2 //Input pin is set to pin 2

volatile long time_interval; //The time interval for which the measurement is performed
volatile long count_edges; //Keeps a count of the number of rising edges
boolean flag; //True when timer has expired, else false
double frequency; //Stores the value of frequency computed
boolean on; //Becomes false after printing frequency once to prevent repetative printing

void setup() 
{//Start of setup
  DDRB&=B11111011; //Sets pin 2 to input
  time_interval=1000000l; //Initializing the interval to 1 second
  count_edges=0l; //Setting the initial number of rising edges to 0
  flag=false; //Timer has not expired, so flag is false
  frequency=0.0; //Frequency is initially stored as 0
  on=true; //Circuit is on (printing enabled)
  
  
  Serial.begin(9600); //Sets up the baud rate for printing the frequency
  
  Timer1.initialize(time_interval); //Initalizes Timer1
  Timer1.attachInterrupt(timeout); //Sets up interrupt for Timer1 expiry
  
  attachInterrupt(digitalPinToInterrupt(INPUT_PIN),edgeDetected,RISING); //Interrupt for rising edge detection
}//End of setup


//ISR for rising edge detection
void edgeDetected()
{//Start of ISR for rising edge detection
  count_edges++; //Increases count of rising edges for every rising edge detected
}//End of ISR for rising edge detection


//ISR for timeout
void timeout()
{//Start of ISR for timeout
  flag=true; //Flag becomes true, indicating expiry of timer
}//End of ISR for timeout


//Main program code
void loop() 
{//Start of loop

  //Print only if timer has expired (flag is true) and printing is enabled (on is true)
  if(flag&&on)
  {//Start of if
    noInterrupts(); //Disabling interrupts while printing to prevent extra edges count
    //Computing frequency by unitary method as the number of edges in 1s (1000000 microseconds)
    frequency=count_edges*1.0/time_interval*1000000;

    //Start of Code for printing frequency
    Serial.print("Frequency: ");
    Serial.print(frequency);
    Serial.println(" Hz");
    //End of Code for printing frequency

    //Resetting the parameters
    count_edges=0;
    flag=false;
    
    on=false; //Once printing is done, do not print again
    
    interrupts(); //Re-enabling interrupts
  }//End of if
}//End of loop
