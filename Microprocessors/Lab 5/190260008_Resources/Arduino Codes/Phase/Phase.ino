/*
 * Program to compute the delay between two input signals
 * 
 * ANKAN MUKHERJEE
 * 190260008
 * IIT BOMBAY
 * PH 230
 * LAB 05
 * Code 02
 */

#include <TimerOne.h>

#define INPUT_PIN_A 2 //Input pin for first signal is set to pin 2
#define INPUT_PIN_B 3 //Input pin for first signal is set to pin 3


long timeA, timeB; //Stores the time at which a rising edge is detected in the corresponding signal for the first time
boolean flag; //True when timer has expired, else false
boolean on; //Becomes false after printing frequency once to prevent repetative printing
double timediff; //Stores the value of time difference computed
    
void setup() 
{//Start of setup
    DDRB&=B11110011; //Sets pins 2 and 3 to input
    timeA=0l; //Initalizes the timers to 0
    timeB=0l; //Initalizes the timers to 0
    flag=false; //Timer has not expired, so flag is false
    timediff=0.0; //Time difference is initially stored as 0
    on=true; //Circuit is on (printing enabled)

    Serial.begin(9600); //Sets up the baud rate for printing the frequency
    
    attachInterrupt(digitalPinToInterrupt(INPUT_PIN_A), ISR_pinA, RISING); //Interrupt for rising edge detection for the first signal
    attachInterrupt(digitalPinToInterrupt(INPUT_PIN_B), ISR_pinB, RISING); //Interrupt for rising edge detection for the second signal
}//End of setup

//ISR for rising edge detection for the first signal
void ISR_pinA()
{//Start of ISR for rising edge detection for the first signal
    timeA = micros(); //Stores the time at which the rising edge of the first signal is detected    
}//End of ISR for rising edge detection for the first signal

//ISR for rising edge detection for the second signal
void ISR_pinB()
{//Start of ISR for rising edge detection for the second signal
    timeB = micros(); //Stores the time at which the rising edge of the second signal is detected    
    flag=true; //Flag becomes true, indicating detection of rising edge
}//End of ISR for rising edge detection for the second signal

//Main program code
void loop() 
{//Start of loop

    //Print only if both signals' rising edges are detected (flag is true) and printing is enabled (on is true)
    if(flag&&on)
    {//Start of if
      noInterrupts(); //Disabling interrupts while printing to prevent extra edges count
      if( timeB >= timeA ) //We want positive time difference
      {//Start of inner if 

        //Start of Code for printing delay     
        Serial.print( "Time between pulses was: " );
        timediff = (timeB - timeA)/1000.0; //Converting delay to milliseconds
        Serial.print(timediff);
        Serial.println(" mS");
        //End of Code for printing delay   
        
        on=false; //Once printing is done, do not print again
      }//End of inner if

        //Resetting the parameters
        flag = false;
        
        interrupts(); //Re-enabling interrupts         
     }//End of if    
}//End of loop
