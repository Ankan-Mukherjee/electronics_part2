Version 4
SHEET 1 880 680
WIRE 336 64 272 64
WIRE 448 64 416 64
WIRE 272 112 272 64
WIRE 448 144 448 64
WIRE 272 240 272 176
WIRE 448 320 448 224
WIRE 272 496 272 320
WIRE 448 496 448 384
WIRE 448 496 272 496
SYMBOL voltage 272 224 R0
WINDOW 123 0 0 Left 0
WINDOW 39 0 0 Left 0
SYMATTR InstName V1
SYMATTR Value 9
SYMBOL diode 288 176 R180
WINDOW 0 24 64 Left 2
WINDOW 3 24 0 Left 2
SYMATTR InstName D1
SYMATTR Value 1N4148
SYMBOL res 432 48 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R1
SYMATTR Value 150
SYMBOL res 432 128 R0
WINDOW 3 48 83 Left 2
SYMATTR Value 6.8k
SYMATTR InstName R2
SYMBOL LED 432 320 R0
SYMATTR InstName D2
SYMATTR Value NSCW100
