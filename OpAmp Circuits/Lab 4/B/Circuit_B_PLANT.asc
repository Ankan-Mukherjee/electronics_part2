Version 4
SHEET 1 880 680
WIRE -384 -208 -432 -208
WIRE -240 -208 -304 -208
WIRE -64 -208 -240 -208
WIRE 64 -208 -64 -208
WIRE 496 -208 64 -208
WIRE -240 -176 -240 -208
WIRE 384 -160 288 -160
WIRE -432 -128 -432 -144
WIRE -64 -128 -64 -208
WIRE 816 -128 448 -128
WIRE 288 -112 288 -160
WIRE 496 -112 496 -208
WIRE 448 -96 448 -128
WIRE 464 -96 448 -96
WIRE -240 -80 -240 -96
WIRE 624 -80 528 -80
WIRE 384 -64 384 -160
WIRE 464 -64 384 -64
WIRE 624 -48 624 -80
WIRE -432 16 -432 -48
WIRE -432 16 -576 16
WIRE -240 16 -240 -16
WIRE -240 16 -432 16
WIRE -64 16 -64 -64
WIRE -64 16 -240 16
WIRE 80 16 -64 16
WIRE 288 16 288 -32
WIRE 288 16 80 16
WIRE -432 80 -432 16
WIRE -240 80 -240 16
WIRE 624 112 624 96
WIRE 816 112 816 -128
WIRE 816 112 624 112
WIRE -64 128 -64 16
WIRE 624 128 624 112
WIRE -240 192 -240 160
WIRE -432 224 -432 160
WIRE 624 256 624 208
WIRE -384 288 -432 288
WIRE -240 288 -240 256
WIRE -240 288 -304 288
WIRE -64 288 -64 192
WIRE -64 288 -240 288
WIRE 48 288 -64 288
WIRE 496 288 496 -48
WIRE 496 288 48 288
FLAG -576 16 0
FLAG 80 16 GROUND
FLAG 64 -208 VCC
FLAG 48 288 -VCC
FLAG 624 256 0
SYMBOL voltage -432 -144 R0
WINDOW 123 0 0 Left 0
WINDOW 39 0 0 Left 0
SYMATTR InstName V1
SYMATTR Value 9
SYMBOL voltage -432 64 R0
WINDOW 123 0 0 Left 0
WINDOW 39 0 0 Left 0
SYMATTR InstName V2
SYMATTR Value 9
SYMBOL diode -416 -144 R180
WINDOW 0 24 64 Left 2
WINDOW 3 24 0 Left 2
SYMATTR InstName D1
SYMATTR Value 1N4148
SYMBOL diode -416 288 R180
WINDOW 0 24 64 Left 2
WINDOW 3 24 0 Left 2
SYMATTR InstName D2
SYMATTR Value 1N4148
SYMBOL res -256 -192 R0
SYMATTR InstName R1
SYMATTR Value 6.8k
SYMBOL res -256 64 R0
SYMATTR InstName R2
SYMATTR Value 6.8k
SYMBOL cap -80 -128 R0
SYMATTR InstName C1
SYMATTR Value 100�
SYMBOL cap -80 128 R0
SYMATTR InstName C2
SYMATTR Value 100�
SYMBOL LED -256 -80 R0
SYMATTR InstName D3
SYMATTR Value NSCW100
SYMATTR Description Diode
SYMATTR Type diode
SYMBOL LED -256 192 R0
SYMATTR InstName D4
SYMATTR Value NSCW100
SYMATTR Description Diode
SYMATTR Type diode
SYMBOL res -288 -224 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R3
SYMATTR Value 150
SYMBOL res -288 272 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R4
SYMATTR Value 150
SYMBOL OpAmps\\OP07 496 -144 R0
SYMATTR InstName U1
SYMBOL LED 608 -48 R0
WINDOW 3 83 45 Left 2
SYMATTR InstName LED
SYMATTR Value NSCW100
SYMATTR Description Diode
SYMATTR Type diode
SYMBOL res 608 112 R0
SYMATTR InstName RCC
SYMATTR Value 3.3k
SYMBOL voltage 288 -128 R0
WINDOW 3 24 44 Left 2
WINDOW 123 0 0 Left 0
WINDOW 39 0 0 Left 0
SYMATTR InstName V
SYMATTR Value ""
SYMBOL res 608 0 R0
SYMATTR InstName RShunt
SYMATTR Value 47
TEXT -596 338 Left 2 !.dc V 0 4 0.1
